<?php
// Llamando a los campos
$nombre = $_POST['nombre'];
$correo = $_POST['correo'];
$telefono = $_POST['telefono'];
$comentario = $_POST['comentario'];
$app = $_POST['app'];
$auto = $_POST['auto'];
$nube = $_POST['nube'];

$tipo;
 
if ($app=="Servicios de DevOps" && $auto!="Servicios de AWS" && $nube!="Desarrollo en la Nube" ) {
    $tipo = "Servicios de DevOps";
} elseif ($app!="Servicios de DevOps" && $auto=="Servicios de AWS" && $nube!="Desarrollo en la Nube"){
    $tipo="Servicios de AWS";
} elseif($app!="Servicios de DevOps" && $auto!="Servicios de AWS" && $nube=="Desarrollo en la Nube") {
    $tipo="Desarrollo en la Nube";
}

$header = 'From: ' . $correo . " \r\n";
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/plain";

$message = "Este mensaje fue enviado por: " . $nombre . " \r\n";
$message .= "Su e-mail es: " . $correo  . " \r\n";
$message .= "Teléfono de contacto: " . $telefono . " \r\n";
$message .= "Mensaje: " . $_POST['comentario'] . " \r\n";
$message .= "Tipo de servicio: " .$tipo. " \r\n";
$message .= "Enviado el: " . date('d/m/Y', time());

$para = '18040015@alumno.utc.edu.mx';
$asunto = 'Contáctame!';

mail($para, $asunto, $message, $header);
header("Location: ../index.html");
?>
