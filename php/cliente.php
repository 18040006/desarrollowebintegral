<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>TechnoWorld</title>

    <link
      href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&display=swap"
      rel="stylesheet"
    />
    <!-- Bulma CDN -->
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bulma@0.9.2/css/bulma.min.css"
    />
    <link
      rel="stylesheet"
      href="css/bulma-carousel.min.css"
    />
    <!-- Fontawesome -->
    <script
      src="https://kit.fontawesome.com/2c36e9b7b1.js"
      crossorigin="anonymous"
    ></script>
    <script
      src="js/bulma-carousel.min.js"
      crossorigin="anonymous"
    ></script>

    <!-- CSS FORM -->
    <link rel="stylesheet" href="/css/form.css">
    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.1/normalize.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">

  </head>
  <?php  
include('conexion.php');
include('bots.php'); 
include 'SED.php';
//PROTEGE LAS RUTAS
session_start();
if (isset($_SESSION['usuario'])) {
    $id_user=$_SESSION['usuario'];
    $pass=$_POST['con'];


    $query=mysqli_query($con, "SELECT * FROM usuarios WHERE usuario = '$id_user';");
    if ($row=mysqli_fetch_array($query))
        {  
          $usuario= $row['usuario']; //NVA VARIABLE-VARIABLE DE BD
          $nombre=$row['nombre'];
          $password=$row['psw'];
          $correo=$row['correo'];
          $telefono=$row['telefono'];

          $claveD= SED::decryption($password);
        } 
}else {
  ?>
    <script type="text/javascript">
      alert("No tienes acceso a esta página");
      window.location="../index.html";
    </script>
  <?php 
}
$id_user=$_SESSION['usuario'];
?>

<body>
<?php 


?>
    <!-- NAVBAR -->
   <nav
   class="navbar is-fixed-top"
   role="navigation"
   aria-label="main navigation"
   >
   <div class="navbar-brand">
     <a class="navbar-item" href="../index.html">
       <img src="../img/main-logo.svg" width="80px" height="120px"/>
     </a>

     <a
       role="button"
       class="navbar-burger"
       aria-label="menu"
       aria-expanded="false"
       data-target="navbarBasicExample"
     >
       <span aria-hidden="true"></span>
       <span aria-hidden="true"></span>
       <span aria-hidden="true"></span>
     </a>
   </div>

   <div id="navbarBasicExample" class="navbar-menu">
     <div class="navbar-start">
       <a class="navbar-item" href="../index.html"> Inicio </a>

       <a class="navbar-item" href="../html/error.html" > Productos </a>

       <a class="navbar-item" href="../html/servicios.html"> Servicios </a>

      <!-- <div class="buttons">
        <a class="navbar-item button is-primary" > Priscila </a>
       </div> -->
     </div>

     <div class="navbar-end">
       <div class="navbar-item">
			<div class="buttons">
			<!-- <a class="button is-primary" >
				<strong>Registro</strong>
			</a> -->
            <a class="navbar-item" > Usuario:  <?php echo $usuario; ?> </a>
			<a class="button is-light" href="cerrar.php"> Cerrar Sesión </a>

			</div>
		</div>
		</div>
	</div>
	</nav>

    <!-- PAGE -->
    <!-- START GRID -->
<!-- DO NOT EDIT THIS PART!! -->
<div class="columns is-mobile">
    <div class="column is-1"></div>
    <div class="column is-10">
<!-- DO NOT EDIT THIS PART!! -->

    <!-- MAIN SECTION -->
    <section class="section is-medium is-primary has-text-centered">
          <h1 class="title" style="font-size:3em;"> Bienvenido a la página de CLIENTE </h1>
          <h2 class="subtitle">
            Contrate un ingeniero de DevOps o un equipo completo que lo ayude a automatizar sus aplicaciones, disminuir el tiempo de 
            comercialización, aumentar la eficiencia y reducir los costos de TI.
          </h2>
          
    </section>

    <section class="py-5" style="margin-top: -30px; background-image: url('img/fondo16.jpeg'); min-height: 10%; background-repeat: no-repeat; background-size: cover, auto; background-attachment: fixed; background-position: center center;">
        <div class="col-xl-3 col-md-3 text-center">
            <div class="col-sm-12">
                <img src="img/clients.png" class="z-depth-1 rounded-circle" alt="Foto de perfil"style="height: 280px; width: 210px;">
            </div>
            <div style="padding-bottom: 30px;">
            <center>                        
             <label style="padding-top: 30px; border-bottom: solid 1px; font-size: 20px" >  <?php echo "Nombre:  ".$nombre; ?> </label><br>
              <a href="" data-toggle="modal" data-target="#modalContactForm ">Actualizar Información</a><br>
            </center>
            </div>
        </div>

    </section>

    <!-- MODAL -->
    <div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h3>Actualizar datos</h3>
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
          </button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>

        <!-- Modal Body -->
        <div class="modal-body">
          <p class="statusMsg"></p>
          <form role="form" enctype="multipart/form-data" action="actualizar.php" method="post">
            <label>Nombre</label><br>
            <div class="input-group mb-3 ">
              <input type="text" class="form-control" value="<?php echo $nombre; ?>" placeholder="Nombre: " name="nombre">
            </div> 
            <label>Correo</label><br>
            <div class="input-group mb-3 ">
              <input type="text" class="form-control" value="<?php echo $correo; ?>" placeholder="Nombre: " name="correo">
            </div> 
            <label>Teléfono</label><br>
            <div class="input-group mb-3 ">
              <input type="text" class="form-control" value="<?php echo $telefono; ?>" placeholder="Nombre: " name="telefono">
            </div> 
            <div class="form-group">
              <label for="inputEmail">Contraseña</label>
              <div class="input-group">
                <input type="text" name="usuario" value="<?php echo $_SESSION['usuario']; ?>" style="display:none;">
                <input class="form-control" type="password" value="<?php echo $claveD; ?>" name="password" id="password">
                <button class="input-group-addon" type="button" onclick="mostrarContrasena()"><i class="far fa-eye"></i></button>
              </div>
            </div>

          </div>

          <!-- Modal Footer -->
          <div class="modal-footer">

            <button type="submit" class="btn btn-primary submitBtn" >Actualizar</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <script>
    function mostrarContrasena(){
      var tipo = document.getElementById("password");
      if(tipo.type == "password"){
        tipo.type = "text";
      }else{
        tipo.type = "password";
      }
    }
  </script>
	
</body>  
</html>