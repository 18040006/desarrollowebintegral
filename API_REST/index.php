<?php
	include 'db.php';
	
	$pdo = new Conexion();
	
	//METODO GET
	if($_SERVER['REQUEST_METHOD'] == 'GET'){
		if(isset($_GET['id_usuario']))
		{
			$sql = $pdo->prepare("SELECT * FROM usuarios WHERE id_usuario=:id_usuario");
			$sql->bindValue(':id_usuario', $_GET['id_usuario']);
			$sql->execute();
			$sql->setFetchMode(PDO::FETCH_ASSOC);
			header("HTTP/1.1 200 OK");
			echo json_encode($sql->fetchAll());
			exit;				
			
			} else {
			
			$sql = $pdo->prepare("SELECT * FROM usuarios");

			$sql->execute();
			$sql->setFetchMode(PDO::FETCH_ASSOC);
			header("HTTP/1.1 200 OK");
			echo json_encode($sql->fetchAll());
			exit;		
		}
	}
	
	//METODO POST
	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{ 

		$sql = "INSERT INTO usuarios (usuario, nombre, psw, correo, telefono, tipoUsuario ) 
               VALUES(:usuario, :nombre, :psw, :correo, :telefono, :tipoUsuario) ";

		$stmt = $pdo->prepare($sql);
       $stmt->bindValue(':usuario', $_POST['usuario']);
		$stmt->bindValue(':nombre', $_POST['nombre']);
        $stmt->bindValue(':psw', $_POST['psw']);
		$stmt->bindValue(':correo', $_POST['correo']);
        $stmt->bindValue(':telefono', $_POST['telefono']);
		$stmt->bindValue(':tipoUsuario', $_POST['tipoUsuario']);

		$stmt->execute();
		$idPost = $pdo->lastInsertId(); 
		if($idPost)
		{
			header("HTTP/1.1 200 Ok");
			echo json_encode("Registrado");
			exit;
		} 

	}

	
	//Si no corresponde a ninguna opción anterior
	header("HTTP/1.1 400 Bad Request");
?>