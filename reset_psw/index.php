<?php 
include	('../php/bots.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Recuperar Contraseña&ntilde;a</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div class="jumbotron text-center" style="background-color:silver" >
        <h1>Recuperando contraseña</h1>
        <p>Por favor, ingrese su nombre de usuario y en seguida le enviaremos un correo.</p>
        </div>
        <div class="container">
        <div class="row">
            <div class="col-sm-4"> </div>
            <div class="col-sm-4 text-center">
                <br>
            <form action="iniciar_recupero.php" method="post">

            <div class="form-group">
                <label for="username">  <h4> <b> Nombre de usuario: </b> </h4> </label>
                <input type="text" name="usuario" id="usuario" class="form-control" required > <br>
                <button type="submit" class="btn btn-primary btn-block">Recuperar contraseña</button>
            </div>
            </form>
            </div>
            <div class="col-sm-4"> </div>
        </div>
    </div>   
</body>
</html>