<?php
require 'composer/vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

if (array_key_exists('usuario', $_POST)) {
    //echo "aqui";
    try {
        $pdo = new PDO(
            'mysql:host=localhost; dbname=saber_hacerdwp',
            'root' 
        );
        $sql = "SELECT correo FROM usuarios WHERE usuario = ?";
        $st = $pdo->prepare($sql);
        $st->bindValue(1, $_POST['usuario']);
        $st->execute();
        if ($result = $st->fetch(PDO::FETCH_ASSOC)) {
            $token = uniqid();
            $correo = $result['correo'];
            //echo "   correo:  ".$correo;
            $sql = "UPDATE usuarios SET token = '$token' WHERE correo = '$correo'"; //{$result['email']};

            try {
                $pdo->exec($sql);
                $mail = new PHPMailer(true);

                try {
                    //Server settings
                    $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
                    $mail->isSMTP();                                            //Send using SMTP
                    $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
                    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
                    $mail->Username   = 'priscilacastrogaray@gmail.com';                     //SMTP username
                    $mail->Password   = '140299pri';                               //SMTP password
                    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                    $mail->Port       = 587;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

                    //Recipients
                    $mail->setFrom( $result['correo'], 'Desarrollo Web Profesional');
                    $mail->addAddress($result['correo']);

                    //Content
                    $mail->isHTML(true);                                  //Set email format to HTML
                    $mail->Subject = 'Recupere su contraseña';
                    $mail->Body    = 'Para recuperar su contraseña favor de seguir las instrucciones.
                    <br> Haga click en <a href="http://localhost/desarrollo-web-profesional-main/reset_psw/recuperar.php?token='.$token.'"> 
                    <b> recuperar mi contraseña </b> </a> .';
                    $mail->AltBody = 'Mensaje enviado para la recuperación de contraseñas, enviado desde phpmailer';
                    $mail->send();
                    ?>  
                    <script type="text/javascript">
                    alert("Hemos enviado un correo a <?php echo $correo; ?> , inténtalo de nuevo.");
                    window.location="../html/login.html";
                    </script>
                    <?php
                } catch (Exception $e) {
                    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                }
            } catch (PDOException $exception) {
                echo 'No pude guardar el token: '.$exception->getMessage();
            }
        } else {
            echo "No existe ese usuario";
        }
    } catch (PDOException $exception) {
        echo "Fallo la conexion a la base: {$exception->getMessage()}";
    }
}
