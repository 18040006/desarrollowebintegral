<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'vendor_name/reset_password',
        'dev' => true,
    ),
    'versions' => array(
        'phpmailer/phpmailer' => array(
            'pretty_version' => 'v6.4.1',
            'version' => '6.4.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpmailer/phpmailer',
            'aliases' => array(),
            'reference' => '9256f12d8fb0cd0500f93b19e18c356906cbed3d',
            'dev_requirement' => false,
        ),
        'vendor_name/reset_password' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
    ),
);
