<?php
include	'../php/bots.php';
include '../php/SED.php';
$pdo = new PDO(
    'mysql:host=localhost; dbname=saber_hacerdwp',
    'root'
);

if (array_key_exists('token', $_GET)) { 
    $sql = "SELECT id_usuario, usuario FROM usuarios WHERE token = ?";
    $st = $pdo->prepare($sql);
    $st->bindValue(1, $_GET['token']);
    $st->execute();
    if ($result=$st->fetch(PDO::FETCH_ASSOC)) {
        $sql = "UPDATE usuarios SET token = null WHERE id_usuario = {$result['id_usuario']};";
        $pdo->exec($sql);
        ?>
         <meta name="viewport" content="width=device-width, initial-scale=1">

         <div class="jumbotron text-center" style="background-color:silver" >
            <h1>Recuperando contraseña</h1>
            <p>Bienvenido <?php echo $result['usuario'];?> </p>
            </div>
            <div class="container">
            <div class="row">
                <div class="col-sm-4"> </div>
                <div class="col-sm-4 text-center">
                    <br>
                <form action="recuperar.php" method="post">
                <div class="form-group">
                <input type="hidden" value="<?php echo $result['id_usuario'];?>" name="uid"/>
                    <label for="newPassword"> <h4> <b> Ingresa tu nueva contrase&ntilde;a: </b> </h4> </label>
                    <input type="password" name="newPassword" id="newPassword" class="form-control" required > <br>
                    <button type="submit" class="btn btn-primary btn-block">Enviar</button>
                </div>
                </form>
                </div>
                <div class="col-sm-4"> </div>
            </div>
        </div>
        <?php
    }
} elseif (array_key_exists('uid', $_POST)) {
    $newPass=$_POST['newPassword'];
    $id1=$_POST['uid'];

    $clave = $newPass;
    $claveE = SED:: encryption($clave);

    $sql = "UPDATE usuarios SET psw = '$claveE' WHERE id_usuario = '$id1';";
    $pdo->exec($sql);
    ?>  
    <script type="text/javascript">
    alert("Contraseña actualizada");
    window.location="../html/login.html";
    </script>
    <?php
}

?>