-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-11-2021 a las 10:42:45
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `saber_hacerdwp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas`
--

CREATE TABLE `citas` (
  `cita_id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidos` varchar(70) DEFAULT NULL,
  `hora_cita` time DEFAULT NULL,
  `fecha_cita` date DEFAULT NULL,
  `fecha_hora` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `citas`
--

INSERT INTO `citas` (`cita_id`, `nombre`, `apellidos`, `hora_cita`, `fecha_cita`, `fecha_hora`) VALUES
(1, 'Guillermo', 'Cazares Ramos', '10:00:00', '2021-07-23', '2021-07-23 10:00:00'),
(2, 'Jorge', 'Quintero', '10:30:00', '2021-07-23', '2021-07-23 10:30:00'),
(3, 'Isaac', 'Alonso', '12:00:00', '2021-07-23', '2021-07-23 12:00:00'),
(4, 'Adela', 'Carrizales', '17:30:00', '2021-07-23', '2021-07-23 17:30:00'),
(5, 'Ana', 'Castillo', '17:45:00', '2021-07-23', '2021-07-23 17:45:00'),
(6, 'Otro', 'Otro', '11:30:00', '2021-07-23', '2021-07-23 11:30:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `farm_inventario`
--

CREATE TABLE `farm_inventario` (
  `PROD_SER` varchar(50) DEFAULT NULL,
  `PROD_MOLECULA` varchar(50) DEFAULT NULL,
  `PROD_GRUPO` varchar(50) DEFAULT NULL,
  `PROD_UBICACION` varchar(50) DEFAULT NULL,
  `PROD_CATEGORIA` varchar(50) DEFAULT NULL,
  `PROD_NOMCATEGORIA` varchar(50) DEFAULT NULL,
  `PROD_LAB` varchar(50) DEFAULT NULL,
  `PROD_PROVEEDOR` varchar(50) DEFAULT NULL,
  `PROD_REFRIGERADO` varchar(50) DEFAULT NULL,
  `PROD_CONTROLADO` varchar(50) DEFAULT NULL,
  `PROD_STOCK` varchar(50) DEFAULT NULL,
  `PROD_CADUCIDAD` varchar(50) DEFAULT NULL,
  `PROD_MAX` varchar(50) DEFAULT NULL,
  `PROD_MIN` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `farm_inventario`
--

INSERT INTO `farm_inventario` (`PROD_SER`, `PROD_MOLECULA`, `PROD_GRUPO`, `PROD_UBICACION`, `PROD_CATEGORIA`, `PROD_NOMCATEGORIA`, `PROD_LAB`, `PROD_PROVEEDOR`, `PROD_REFRIGERADO`, `PROD_CONTROLADO`, `PROD_STOCK`, `PROD_CADUCIDAD`, `PROD_MAX`, `PROD_MIN`) VALUES
('a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'),
('b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b'),
('c', 'c', 'c', 'c', 'c', 'c', 'c', 'c', 'c', 'c', 'c', 'c', 'c', 'c'),
('d', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd'),
('E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'),
('F', 'FF', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F'),
('FG', 'G', 'G', 'G', 'G', 'G', 'G', 'GG', 'G', 'G', 'G', 'G', 'G', 'G'),
('H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H'),
('I', 'I', 'I', 'I', 'I', 'II', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I'),
('J', 'J', 'J', 'J', 'J', 'J', 'J', 'J', 'J', 'J', 'J', 'J', 'J', 'J'),
('J', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K'),
('L', 'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L'),
('LM', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'MN'),
('NN', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N'),
('N', 'N', 'P', 'P', 'P', 'P', 'P', 'P', 'P', 'P', 'P', 'P', 'P', 'P'),
('P', 'P', 'P', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O'),
('O', 'OU', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'UUU', 'U', 'U'),
('U', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `psw` varchar(50) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `tipoUsuario` varchar(50) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `usuario`, `nombre`, `psw`, `correo`, `telefono`, `tipoUsuario`, `token`) VALUES
(1, 'priscilacastro', 'Priscila Castro', 'bHA0S1J1eXBZTTVhd21vTnEycVI5dz09', 'pc513401@gmail.com', '8443554428', 'proveedor', NULL),
(16, 'Fatima123', 'Fatima Arriaga', 'e10adc3949ba59abbe56e057f20f883e', 'fatima@gmail.com', '8442347801', 'cliente', ''),
(17, 'Bala_Giron', 'Bala Giron', 'V0Z0MjdvUTJoU294aTZvd2NvYjQrdz09', '18040015@alumno.utc.edu.mx', '8447230129', 'cliente', NULL),
(49, 'PedroMendoza', 'Pedro Mendoza', 'bHA0S1J1eXBZTTVhd21vTnEycVI5dz09', 'pedromendoza@gmail.com', '8442378901', 'cliente', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
